package org.i9.slb.platform.anseko.provider.dto;

import java.io.Serializable;
import java.util.HashMap;

public class KeyValuesDto implements Serializable {

    private static final long serialVersionUID = 9168005848754171350L;

    private HashMap<String, Object> parameters = new HashMap<String, Object>();

    public HashMap<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(HashMap<String, Object> parameters) {
        this.parameters = parameters;
    }

    public Object getParameter(String name) {
        return parameters.get(name);
    }

    public void setParameter(String name, Object value) {
        parameters.put(name, value);
    }
}
