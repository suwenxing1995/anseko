package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.InstanceDto;

import java.util.List;

/**
 * 实例远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:37
 */
public interface IDubboInstanceRemoteService {

    /**
     * 获取实例信息
     *
     * @param id
     * @return
     */
    InstanceDto getInstanceDtoInfo(String id);

    /**
     * 获取实例列表
     *
     * @return
     */
    List<InstanceDto> getInstanceDtoList();

    /**
     * 创建实例
     *
     * @param instanceDto
     */
    void createInstanceInfo(InstanceDto instanceDto);
}
