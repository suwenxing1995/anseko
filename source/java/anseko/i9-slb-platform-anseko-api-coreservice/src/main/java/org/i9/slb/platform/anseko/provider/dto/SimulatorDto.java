package org.i9.slb.platform.anseko.provider.dto;

import org.i9.slb.platform.anseko.common.PowerState;

/**
 * 模拟器dto
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class SimulatorDto implements java.io.Serializable {

    public PowerState getPowerState() {
        return PowerState.valueOf(this.powerStatus);
    }

    private static final long serialVersionUID = -3963469857629881946L;

    private String id;

    private String simulatorName;

    private String createDate;

    private Integer cpunum;

    private Integer ramnum;

    private Integer powerStatus;

    private String vncpassword;

    private Integer vncport;

    private String androidVersion;

    private String instance;

    private InstanceDto instanceDto;

    public InstanceDto getInstanceDto() {
        return instanceDto;
    }

    public void setInstanceDto(InstanceDto instanceDto) {
        this.instanceDto = instanceDto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorName() {
        return simulatorName;
    }

    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    public Integer getCpunum() {
        return cpunum;
    }

    public void setCpunum(Integer cpunum) {
        this.cpunum = cpunum;
    }

    public Integer getRamnum() {
        return ramnum;
    }

    public void setRamnum(Integer ramnum) {
        this.ramnum = ramnum;
    }

    public Integer getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(Integer powerStatus) {
        this.powerStatus = powerStatus;
    }

    public String getVncpassword() {
        return vncpassword;
    }

    public void setVncpassword(String vncpassword) {
        this.vncpassword = vncpassword;
    }

    public Integer getVncport() {
        return vncport;
    }

    public void setVncport(Integer vncport) {
        this.vncport = vncport;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
