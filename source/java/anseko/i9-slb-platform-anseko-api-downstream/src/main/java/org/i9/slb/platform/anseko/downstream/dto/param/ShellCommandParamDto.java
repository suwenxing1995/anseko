package org.i9.slb.platform.anseko.downstream.dto.param;

import java.util.UUID;

/**
 * shell命令执行传输对象
 *
 * @author R12
 * @date 2018.08.28
 */
public class ShellCommandParamDto extends CommandParamDto implements java.io.Serializable {

    public static ShellCommandParamDto build(String commandLine) {
        ShellCommandParamDto shellCommandParamDto = new ShellCommandParamDto();
        shellCommandParamDto.setCommandId(UUID.randomUUID().toString());
        shellCommandParamDto.setCommandLine(commandLine);
        return shellCommandParamDto;
    }

    private static final long serialVersionUID = 3865414192739068016L;
}
