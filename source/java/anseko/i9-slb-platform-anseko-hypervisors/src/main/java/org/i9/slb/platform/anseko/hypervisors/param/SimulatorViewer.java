package org.i9.slb.platform.anseko.hypervisors.param;

/**
 * 虚拟化模拟器vnc描述
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 11:20
 */
public class SimulatorViewer {

    /**
     * 访问端口
     */
    private int port;

    /**
     * 访问密码
     */
    private String password;

    /**
     * 虚拟化模拟器vnc描述构造函数
     *
     * @param port
     * @param password
     */
    public SimulatorViewer(int port, String password) {
        this.port = port;
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
