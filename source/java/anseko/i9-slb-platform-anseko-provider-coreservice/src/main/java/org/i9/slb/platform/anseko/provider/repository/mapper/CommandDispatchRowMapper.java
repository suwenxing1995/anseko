package org.i9.slb.platform.anseko.provider.repository.mapper;

import org.i9.slb.platform.anseko.provider.dto.CommandDispatchDto;
import org.i9.slb.platform.anseko.common.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * CommandDispatch数据映射
 *
 * @author R12
 * @date 2018年9月6日 16:50:12
 */
public class CommandDispatchRowMapper implements RowMapper<CommandDispatchDto> {

    @Override
    public CommandDispatchDto mapRow(ResultSet resultSet, int i) throws SQLException {
        CommandDispatchDto commandDispatchDto = new CommandDispatchDto();
        commandDispatchDto.setCommandGroupId(resultSet.getString("commandGroupId"));
        commandDispatchDto.setSimulatorId(resultSet.getString("simulatorId"));
        Date startDate = resultSet.getDate("startDate");
        if (startDate != null) {
            commandDispatchDto.setStartDate(DateUtil.format(startDate));
        }
        Date endDate = resultSet.getDate("endDate");
        if (endDate != null) {
            commandDispatchDto.setEndDate(DateUtil.format(endDate));
        }
        commandDispatchDto.setSuccess(resultSet.getInt("success"));
        return commandDispatchDto;
    }
}
