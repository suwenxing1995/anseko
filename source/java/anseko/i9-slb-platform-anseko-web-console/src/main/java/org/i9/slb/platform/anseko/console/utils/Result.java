package org.i9.slb.platform.anseko.console.utils;

/**
 * 操作返回值
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 14:44
 */
public class Result {

    private Integer result;

    private String message;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
