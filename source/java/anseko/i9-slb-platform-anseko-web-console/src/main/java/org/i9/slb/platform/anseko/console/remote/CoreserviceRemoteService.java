package org.i9.slb.platform.anseko.console.remote;

import org.i9.slb.platform.anseko.downstream.dto.param.CommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.provider.IDubboCommandRemoteService;
import org.i9.slb.platform.anseko.provider.IDubboInstanceRemoteService;
import org.i9.slb.platform.anseko.provider.dto.CommandDispatchDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 调用coreservice远程服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 18:16
 */
@Service
public class CoreserviceRemoteService {

    @Autowired
    private IDubboCommandRemoteService dubboCommandRemoteService;

    @Autowired
    private IDubboInstanceRemoteService dubboInstanceRemoteService;

    /**
     * 调用远程服务发起一个调度任务
     *
     * @param simulatorId
     * @param groupCommandParamDto
     */
    public void remoteServiceLaunchCommandDispatch(String simulatorId, GroupCommandParamDto groupCommandParamDto) {
        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        for (CommandParamDto commandParamDto : groupCommandParamDto.getCommands()) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            commandExecuteDto.setCommandId(commandParamDto.getCommandId());
            commandExecuteDto.setCommandLine(commandParamDto.getCommandLine());
            commandExecuteDtos.add(commandExecuteDto);
        }
        CommandDispatchDto commandDispatchDto = new CommandDispatchDto();
        commandDispatchDto.setCommandGroupId(groupCommandParamDto.getGroupId());
        commandDispatchDto.setSimulatorId(simulatorId);
        commandDispatchDto.setCommandExecuteDtos(commandExecuteDtos);
        dubboCommandRemoteService.launchCommandDispatch(commandDispatchDto);
    }

    /**
     * 调用远程服务发起一个调度任务
     *
     * @param simulatorId
     * @param shellCommandParamDto
     */
    public void remoteServiceLaunchCommandDispatch(String simulatorId, ShellCommandParamDto shellCommandParamDto) {
        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
        commandExecuteDto.setCommandId(shellCommandParamDto.getCommandId());
        commandExecuteDto.setCommandLine(shellCommandParamDto.getCommandLine());
        commandExecuteDtos.add(commandExecuteDto);
        CommandDispatchDto commandDispatchDto = new CommandDispatchDto();
        commandDispatchDto.setCommandExecuteDtos(commandExecuteDtos);
        commandDispatchDto.setCommandGroupId(shellCommandParamDto.getCommandId());
        commandDispatchDto.setSimulatorId(simulatorId);
        dubboCommandRemoteService.launchCommandDispatch(commandDispatchDto);
    }

    /**
     * 调用远程服务发起一个调度任务
     *
     * @param simulatorId
     * @param shellCommandParamDtos
     */
    public void remoteServiceLaunchCommandDispatch(String simulatorId, List<ShellCommandParamDto> shellCommandParamDtos) {
        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        for (ShellCommandParamDto shellCommandParamDto : shellCommandParamDtos) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            commandExecuteDto.setCommandId(shellCommandParamDto.getCommandId());
            commandExecuteDto.setCommandLine(shellCommandParamDto.getCommandLine());
            commandExecuteDtos.add(commandExecuteDto);
        }
        CommandDispatchDto commandDispatchDto = new CommandDispatchDto();
        commandDispatchDto.setCommandGroupId(UUID.randomUUID().toString());
        commandDispatchDto.setSimulatorId(simulatorId);
        commandDispatchDto.setCommandExecuteDtos(commandExecuteDtos);
        dubboCommandRemoteService.launchCommandDispatch(commandDispatchDto);
    }

    /**
     * 调用远程服务获取实例地址
     *
     * @param instance
     * @return
     */
    public String remoteServiceGetInstanceRemoteAddress(String instance) {
        InstanceDto instanceDto = this.dubboInstanceRemoteService.getInstanceDtoInfo(instance);
        return instanceDto.getRemoteAddress();
    }

    /**
     * 调用远程服务获取实例详情
     *
     * @param instance
     * @return
     */
    public InstanceDto remoteServiceGetInstanceDtoInfo(String instance) {
        InstanceDto instanceDto = this.dubboInstanceRemoteService.getInstanceDtoInfo(instance);
        return instanceDto;
    }
}
