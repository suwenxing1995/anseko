package org.i9.slb.platform.anseko.hypervisors.kvm;

import org.i9.slb.platform.anseko.hypervisors.AbstractSimulatorOperate;

/**
 * kvm虚拟化操作类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 14:51
 */
public class KVMSimulatorOperate extends AbstractSimulatorOperate {
    /**
     * kvm虚拟化操作类构造函数
     *
     * @param name
     */
    public KVMSimulatorOperate(String name) {
        super(name);
    }

    /**
     * 启动模拟器
     *
     * @param name
     */
    @Override
    public String startSimulator(String name) {
        return "virsh start " + name;
    }

    /**
     * 重启模拟器
     *
     * @param name
     */
    @Override
    public String[] rebootSimulator(String name) {
        String[] commands = new String[2];
        commands[0] = this.shutdownSimulator();
        commands[1] = this.startSimulator();
        return commands;
    }

    /**
     * 关闭模拟器
     *
     * @param name
     */
    @Override
    public String shutdownSimulator(String name) {
        return "virsh destroy " + name;
    }

    /**
     * 取消定义模拟器命令
     *
     * @return
     */
    @Override
    public String[] destroySimulator() {
        String[] commands = new String[3];
        commands[0] = this.shutdownSimulator();
        commands[1] = "lvremove -f /dev/disk1/" + name;
        commands[2] = "virsh undefine " + this.name;
        return commands;
    }

    /**
     * 创建物理磁盘
     *
     * @return
     */
    @Override
    public String[] createDiskPath() {
        String[] commands = new String[2];
        commands[0] = "lvcreate -L 3G -n " + this.name + " disk1";
        commands[1] = "dd if=/dev/disk1/android.template.idc9000.com of=/dev/disk1/" + this.name + " bs=1G count=1000";
        return commands;
    }
}
