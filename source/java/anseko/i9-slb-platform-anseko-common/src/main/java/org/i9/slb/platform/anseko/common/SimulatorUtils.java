package org.i9.slb.platform.anseko.common;

import java.util.Random;

/**
 * SimulatorUtils 工具类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/31 16:10
 */
public class SimulatorUtils {

    /**
     * 随机一个定义开头的MAC地址
     *
     * @param first
     * @return
     */
    public static String randomMAC(int first[]) {
        final int size = 6;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < first.length; i++) {
            sb.append(":" + convertHexFormat(first[i]));
        }
        Random random = new Random();
        for (int i = first.length; i < size; i++) {
            String hex = convertHexFormat(random.nextInt(0xFF) + 1);
            sb.append(":" + hex);
        }
        return sb.substring(1);
    }

    /**
     * 随机一个MAC地址
     *
     * @return
     */
    public static String randomMAC() {
        final int size = 5;
        StringBuffer sb = new StringBuffer("0E");
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            String hex = convertHexFormat(random.nextInt(0xFF) + 1);
            sb.append(":" + hex);
        }
        return sb.toString();
    }

    /**
     * 将数字转换为16进制
     *
     * @param next
     * @return
     */
    private static String convertHexFormat(int next) {
        if (next >= 16) {
            return Integer.toHexString(next);
        } else {
            return "0" + Integer.toHexString(next);
        }
    }
}
