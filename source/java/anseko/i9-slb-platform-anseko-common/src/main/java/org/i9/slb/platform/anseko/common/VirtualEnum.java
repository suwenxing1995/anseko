package org.i9.slb.platform.anseko.common;

/**
 * 虚拟化类型
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:32
 */
public enum VirtualEnum {

    KVM, XEN, VMWARE, VIRTUALBOX;

    public static VirtualEnum valueOf(int value) {
        value -= 1;
        for (VirtualEnum virtualEnum : values()) {
            if (virtualEnum.ordinal() == value) {
                return virtualEnum;
            }
        }
        return VirtualEnum.KVM;
    }
}
